import src.configuration
import logging as log
import requests


class JukeboxConfigServiceProxy:
    __url_base = ''
    __url_keys = __url_base + '/jukeboxitem/keys/'

    def __init__(self, appConfig):
        self.__url_base = appConfig.getJukeboxConfigServiceUrl()

    # Will return config item for key press.  if doesnt exist then returns `None`.
    def getConfig(self, keysPressed):
        fullUrl = self.__url_keys + keysPressed
        log.info('calling for values on ' + fullUrl)
        try:
            req = requests.get(url=fullUrl)
            data = req.json()
            return KeyPressMapping(data['keys'], data['service'], data['servicekey'])
        except Exception as err:
            log.warning('Failed to load config back (will return None) {0}'.format(err))
            return None




class KeyPressMapping:
    __keys = ''
    __service = ''
    __serviceMapping = ''

    def __init__(self, keys, service, mapping):
        self.__keys = keys
        self.__service = service
        self.__serviceMapping = mapping
    
    def getKeys(self):
        return self.__keys

    def getService(self):
        return self.__service

    def getServiceMapping(self):
        return self.__serviceMapping
