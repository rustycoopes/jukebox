import logging as log
import logging.config as logConfig
import configparser
from sys import path
import os


myPath = os.path.dirname(__file__)


class ApplicationConfiguration:
    __configServiceUrl=''

    def __init__(self):
        appConfigFile = os.path.join(myPath, '../config/', 'router.ini')
        logConfigFile = os.path.join(myPath,  '../config/', 'logging.conf')
        if not os.path.exists(appConfigFile):
            raise FileNotFoundError(appConfigFile)
        if not os.path.exists(logConfigFile):
            raise FileNotFoundError(logConfigFile)


        logConfig.fileConfig(logConfigFile )
        log.debug('Created logger')      
        
        config = configparser.ConfigParser()
        config.read(appConfigFile)
        self.__configServiceUrl = config['config-service']['url']
        self.logConfig()

    def logConfig(self):
        log.info('Current config:')
        log.info('JukeboxConfigServiceUrl = ' + self.__configServiceUrl)

    def getJukeboxConfigServiceUrl(self):
        return self.__configServiceUrl


       