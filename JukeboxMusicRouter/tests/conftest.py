
import shutil
from sys import path
import os
additionalPath = os.path.realpath('.\\src\\')
path.append(additionalPath)
myPath = os.path.dirname(__file__)

def pytest_configure(config):
        sourceFile = os.path.join(myPath, '../config/', 'test.ini')
        destinationFile = os.path.join(myPath, '../config/', 'router.ini')
        shutil.copyfile(sourceFile,destinationFile)
