
import json
import pytest_mock
import pytest
import requests 
import src.configuration
import src.JukeboxConfigServiceProxy as proxy


def test_runEmptyTest():
    assert 1 == 1


def test_callservernoMappingFound(mocker):

    resp = requests.Response()
    mocker.patch('requests.get', return_value=resp)
    mocker.patch('requests.Response.json', return_value=None)
    config = src.configuration.ApplicationConfiguration()
    server = proxy.JukeboxConfigServiceProxy(config)
    configItem  = server.getConfig(keysPressed='A3')
    assert configItem == None



def test_callservertoReadKey(mocker):

    message = json.loads('{"keys":"A3", "service":"pandora", "servicekey":"akey"}')
    resp = requests.Response()
    mocker.patch('requests.get', return_value=resp)
    mocker.patch('requests.Response.json', return_value=message)
    config = src.configuration.ApplicationConfiguration()
    server = proxy.JukeboxConfigServiceProxy(config)
    configItem  = server.getConfig(keysPressed='A3')
    assert configItem.getKeys() == 'A3'
    assert configItem.getService() == 'pandora'
    assert configItem.getServiceMapping() == 'akey'


#def test_callToServerIntegration():

 #   server = src.JukeboxConfigServiceWrapper.JukeboxConfigServiceWrapper()
  #  configItem  = server.getConfig(keysPressed='A1')
   # assert configItem.getKeys() == 'A1'
    #assert configItem.getService() == 'Pandora'
    #assert configItem.getServiceMapping() == 'akey'
