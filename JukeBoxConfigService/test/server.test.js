const app = require("../server");
const db = require("../database");
const chai = require("chai");
const chaiHttp = require("chai-http");

const { expect } = chai;
chai.use(chaiHttp);

before(async () => {  
  await deleteTestData()
})

after(async () => {  
  await deleteTestData()
})


function deleteTestData(){
  try{
    var fs = require('fs');
    var filePath = db.getDatabaseFileName()
    fs.unlinkSync(filePath);
  }
  catch(err){}
}


describe("JukeboxConfigServer!", () => {
  it("Can add jukeboxitem", done => {
    chai
      .request(app)
      .post("/jukeboxitem")
      .send({ keys: 'A1', service: 'Pandora', servicekey: 'ironmaiden' })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.equals("Jukebox Item created successfully");
        done();
      });
  });
      
  it("Can get jukeboxitem", done => {
      chai
        .request(app)
        .get("/jukeboxitem/1")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.keys).to.equals("A1");
          expect(res.body.service).to.equals("Pandora");
          expect(res.body.servicekey).to.equals("ironmaiden");
          done();
        });
  });
  it("Can get all jukeboxitem", done => {
    chai
      .request(app)
      .get("/jukeboxitems/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.length).to.equals(1);
        expect(res.body[0].keys).to.equals("A1");
        expect(res.body[0].service).to.equals("Pandora");
        expect(res.body[0].servicekey).to.equals("ironmaiden");
        done();
      });
  });
  it("Can update jukeboxitem", done => {
    chai
      .request(app)
      .patch("/jukeboxitem/1")
      .send({ keys: 'A1', service: 'Pandora', servicekey: 'acdc' })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.equals("Updated Successfully");
        done();
      });
  });
  it("Can get updated jukeboxitem", done => {
    chai
      .request(app)
      .get("/jukeboxitem/1")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.keys).to.equals("A1");
        expect(res.body.service).to.equals("Pandora");
        expect(res.body.servicekey).to.equals("acdc");
        done();
      });
  });
  it("Can get item by key", done => {
    chai
      .request(app)
      .get("/jukeboxitem/keys/A1")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.keys).to.equals("A1");
        expect(res.body.service).to.equals("Pandora");
        expect(res.body.servicekey).to.equals("acdc");
        done();
      });
  });
  it("Can delete jukeboxitem", done => {
    chai
      .request(app)
      .delete("/jukeboxitem/1")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.equals("Deleted Successfully");
        done();
      });
  });
  it("Can confirm jukeboxitem was removed", done => {
    chai
      .request(app)
      .get("/jukeboxitems/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.length).to.equals(0);
        done();
      });
  });
});

