const db = require("../database");
const {describe} = require('mocha');
var appRoot = require('app-root-path');
const {expect} = require('chai');

describe('JsonFilebasedDatabase!', function() {
    it('Can save mulitple entries to file', function() {
        data = [{keys:"A1",service:"Pandora",servicekey:"ironmaiden",id:1,date:"1577111675243"},
        {keys:"A2",service:"Pandora",servicekey:"somethingelse",id:2,date:"1577111675243"}];
        db.setData(data);
 
    });
    it('Can load data from file', function() {
        data = db.loadData();
        expect(data.length).to.equal(2);
    });
});

        