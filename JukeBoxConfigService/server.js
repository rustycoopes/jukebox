'use strict';
const express = require("express");
const bodyparser = require("body-parser");
var winston = require('./config/winston');
const db = require("./database");
let config = require('config');
// defaults

let port = process.env.PORT || 3200;

if (config.has('webserver.port')) {
    port= config.get('webserver.port');
}

/***
 * Create the express endpoint server
 */
const app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.listen(port, () => {
  winston.log('info', `Environment set to ${config.util.getEnv('NODE_ENV')}, running at port ${port}` , );
});
module.exports = app;



let jukeboxitems = db.loadData();
/**
 * REST SERVICE
 */
/**
 * creating a Config item
 */

app.post("/jukeboxitem", (req, res) => {
  const jukeboxitem = req.body;

  if (jukeboxitem.keys || jukeboxitem.service || jukeboxitem.servicekey) {
    jukeboxitems.push({
      ...jukeboxitem,
      id: jukeboxitems.length + 1,
      date: Date.now().toString()
    });
  
    db.setData(jukeboxitems)

    winston.log('info', `Added new item ${JSON.stringify(jukeboxitem)}`, )
  
    res.status(200).json({message: "Jukebox Item created successfully"});

  } else {
    res.status(401).json({message: "Invalid Jukebox item creation"});
  }
});

/**
 *  Getting jukeboxitems
 */

app.get("/jukeboxitems", (req, res) => {

  winston.log('info', `Returning ${jukeboxitems.length} items`, )
  
  res.status(200).send(jukeboxitems);
});

app.get("/jukeboxitem/:id", (req, res) => {
    if (jukeboxitems.length == 0){
        res.status(404).json({message:"No Jukebox items have been added yet"});
        return;
    }
  const jukeboxitem_id = req.params.id;
  for (let jukeboxitem of jukeboxitems) {
    if (jukeboxitem.id == jukeboxitem_id) {
        res.status(200).send(jukeboxitem);
        return;
    }
  }
  res.status(404).json({ message: "Invalid Jukebox item Id" });
});


app.get("/jukeboxitem/keys/:key", (req, res) => {
  if (jukeboxitems.length == 0){
      res.status(404).json({message:"No Jukebox items have been added yet"});
      return;
  }
const jukeboxitem_keys = req.params.key;

winston.log('info', `Finding config item by key  ${jukeboxitem_keys} `, )
for (let jukeboxitem of jukeboxitems) {
  if (jukeboxitem.keys == jukeboxitem_keys) {
      res.status(200).send(jukeboxitem);
      return;
  }
}
res.status(404).json({ message: "Invalid Jukebox item Id" });
});


/**
 * Update order
 */
app.patch("/jukeboxitem/:id", (req, res) => {
  const jukeboxitem_id = req.params.id;
  const jukeboxitem_update = req.body;
  for (let jukeboxitem of jukeboxitems) {
    if (jukeboxitem.id == jukeboxitem_id) {
      if (jukeboxitem_update.keys != null || undefined)
      jukeboxitem.keys = jukeboxitem_update.keys;
      if (jukeboxitem_update.service != null || undefined)
      jukeboxitem.service = jukeboxitem_update.service;
      if (jukeboxitem_update.servicekey != null || undefined)
      jukeboxitem.servicekey = jukeboxitem_update.servicekey;

      db.setData(jukeboxitems)
      winston.log('info', `Updated existing item to: ${JSON.stringify(jukeboxitem)}`, )

      return res
        .status(200)
        .json({ message: "Updated Successfully", data: jukeboxitem });
    }
  }

  res.status(404).json({ message: "Invalid Jukebox item Id" });
});

/**
 * Delete Order
 */
app.delete("/jukeboxitem/:id", (req, res) => {
  const jukeboxitem_id = req.params.id;

  let foundItem =  null;
  for (let jukeboxitem of jukeboxitems) {
    if (jukeboxitem.id == jukeboxitem_id) {
        foundItem = jukeboxitem;
        break;
    }
  }
  
  if(foundItem != null){
    jukeboxitems.splice(jukeboxitems.indexOf(foundItem), 1);
    db.setData(jukeboxitems)
    winston.log('info', `Deleted item ${JSON.stringify(foundItem)}`, )

    return res.status(200).json({message: "Deleted Successfully"});
  } 

  res.status(404).json({ message: "Invalid Jukebox item Id" });
});

