'use strict';
var winston = require('./config/winston');
var appRoot = require('app-root-path');
let config = require('config');
const fs = require('fs');



module.exports.loadData = function(overrideFileName = '') {

    let databaseFileName = this.getDatabaseFileName(overrideFileName);

    let jsonData =[]
    
    if(fs.existsSync(databaseFileName)){
        jsonData = require(databaseFileName);
        winston.log('info', `Loaded ${databaseFileName}, reading file :${jsonData.length} records loaded`);
    }

    return jsonData;    
};


module.exports.setData = function(jsonPayLoad) {
   
    let databaseFileName = this.getDatabaseFileName();
    let data = JSON.stringify(jsonPayLoad);

    if(fs.existsSync(databaseFileName))
        fs.writeFileSync(databaseFileName, data);
    else
        fs.writeFileSync(databaseFileName, data,{flag:'wx'});

    winston.log('debug', `Data written to file ${data}`);
};



module.exports.getDatabaseFileName =function getDatabaseFileName(overrideFileName = ''){

    let databaseFileName = overrideFileName;
   
    if(databaseFileName == '')
    {
        if (config.has('database.filename')) {
            databaseFileName= `${appRoot}${config.get('database.filename')}`;
            winston.log('debug', `Found database config`);
        }
    }
 
    winston.log('debug', `Database from set to ${databaseFileName}`);
 
    return databaseFileName;
}
