var appRoot = require('app-root-path');
var win = require('winston');

// define the custom settings for each transport (file, console)
var options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/server.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: 'info',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

// instantiate a new Winston Logger with the settings defined above
var logger = new win.createLogger({
  transports: [
    new win.transports.File(options.file),
    new win.transports.Console(options.console)
  ],
  exitOnError: false, // do not exit on handled exceptions
});


module.exports = logger;