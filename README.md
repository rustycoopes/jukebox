# Jukebox
Project to turn an oldschool jukebox into something which can communicat with modern app services and players
e.g. Spotify through Sonos

## Components

### JukeboxButtonPressPublisher
This is a interface from the jukebox itself, and sends key press messages via Zero MQ

### MusicPlayerRouter
This takes publications from the Publisher, and sends instructions to the appropriate Music Player Service
e.g. take key press from jukebox. Look up what(playlist) to play, from where (music service) and on what (player)

### MusicPlayerService(s)
Will be composed of a* ***MusicProvider**** which represents the online Music Provider, e.g. Pandora, Spotify etc
and a* ***MusicSoundSystem**** which represents the Physical sound system, e.g. Sonos.  Each service implementation uses a single interface - invoked from the router.

### JukeBoxConfigService
This will hold the shared data configuration for the outer and music playerservice, exposed via a rest api. This is managed by the admin webservice.

### JukeBoxAdmin 
This offers a tool to manage the mappings held in the config service


## Running the environment
Below assumes repo is checked out to a local folder.
Commands are issued from the folder of the service (not the repo root)

### Jukebox config service
To install
* [ ] `npm install`
* [ ] `npm tests`.

To Run 
* [ ] Dump data into environment run `npm run database-copy` (*optional*)
* [ ] To run test `npm start`  
* [ ] To run prod `npm run start-prod`

### Music Router
To install, ensure **python 3.6** up, plus **pipenv**

To install
* [ ] `pipenv install`
* [ ] `pytest`

To Run
* [ ] Make sure the appropriate environments config file is set as router.ini.  **Unit tests will copy test.ini onto router.ini**, before execution.  If you want to run **prod** or dev - copy files over that.
* [ ] pipenv run JukeboxKeyPressRouter.py


